from utils.faker import fake


class Valid:
    STATIC_USERNAME = fake.password(length=5, special_chars=False, digits=True, upper_case=True, lower_case=True)
    STATIC_PASSWORD = fake.password(length=10, special_chars=True, digits=True, upper_case=True, lower_case=True)

    @staticmethod
    def generate_username() -> str:
        return fake.password(length=5, special_chars=False, digits=True, upper_case=True, lower_case=True)

    @staticmethod
    def generate_password() -> str:
        return fake.password(length=10, special_chars=True, digits=True, upper_case=True, lower_case=True)
