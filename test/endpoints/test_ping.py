from main import app
from test.endpoints.client import client


def test_1_get_ping():
    response = client.get(app.url_path_for('ping'))
    assert response.status_code == 200
    assert response.json() == {"message": "Pong"}
