import aiohttp
import pytest
import pytest_asyncio

from core.database import get_session
from main import app
from models import User
from schemas import PydanticSmallUser, PydanticBigUser
from test.settings import TestSettings
from test.valid import Valid
from utils.helpers import get_clean_path

SIGN_UP_URL = get_clean_path(TestSettings.URL, app.url_path_for('sign_up'))
LOGIN_URL = get_clean_path(TestSettings.URL, app.url_path_for('login'))
DEACTIVATION_URL = get_clean_path(TestSettings.URL, app.url_path_for('deactivate_user'))


@pytest.mark.asyncio
async def test_1_sign_up_new_user_and_login_get_user_success():
    username, password = Valid.generate_username(), Valid.generate_password()
    sign_up_data = {"username": username, "password": password, "confirm_password": password}
    login_data = {"username": username, "password": password}
    user_id: int
    async with aiohttp.ClientSession() as session:

        async with session.post(SIGN_UP_URL, json=sign_up_data) as response:
            assert response.status == 201

        async with session.post(LOGIN_URL, json=login_data) as response:
            assert response.status == 200
            result = await response.json()

            login_model = PydanticSmallUser(**result)
            user_id = login_model.id
            assert user_id

        generated_user_url = get_clean_path(TestSettings.URL, app.url_path_for('get_user', user_id=user_id))
        async with session.get(generated_user_url) as response:
            assert response.status == 200
            result = await response.json()
            full_model = PydanticBigUser(**result)
            assert login_model.id == full_model.id

        async with session.post(DEACTIVATION_URL, json={"user_id": user_id}) as response:
            assert response.status == 200
            result = await response.json()
            assert result == {"success": "Successfully deactivated"}
