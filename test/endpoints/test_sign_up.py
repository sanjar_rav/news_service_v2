from main import app
from test.endpoints.client import client
from test.valid import Valid


def test_1_sign_up_new_user_success():
    sign_up_data = {
        "username": Valid.generate_username(),
        "password": Valid.STATIC_PASSWORD,
        "confirm_password": Valid.STATIC_PASSWORD
    }
    response = client.post(app.url_path_for('sign_up'), json=sign_up_data)
    assert response.status_code == 201
