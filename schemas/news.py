from dataclasses import dataclass
from typing import Optional
from pydantic import BaseModel
from datetime import datetime
from fastapi import Form


class NewsList(BaseModel):
    id: int
    created_at: datetime
    updated_at: datetime
    send_date: datetime
    title: str
    description: str
    media: Optional[str]
    status: str
    send_now: bool
    is_show = bool

    class Config:
        orm_mode = True


@dataclass
class SimpleModel:
    title: str = Form(...)
    description: str = Form(...)
    status: str = Form(default='is_unread')
    send_date: datetime = Form(...)
    send_now: bool = Form(...)


class NewsUpdate(BaseModel):
    title: str = None
    description: str = None
    status: str = None
    send_date: datetime = None
    send_now: bool = None
    is_show: bool = None
