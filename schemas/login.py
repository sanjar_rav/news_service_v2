from pydantic import BaseModel


class LoginUserSchema(BaseModel):
    username: str
    password: str
