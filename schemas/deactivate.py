from pydantic import BaseModel


class DeactivationSchema(BaseModel):
    user_id: int
