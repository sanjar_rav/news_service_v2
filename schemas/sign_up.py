from typing import List, Optional

from pydantic import BaseModel

from auth.roles import Roles


class CreateUserSchema(BaseModel):
    username: str
    password: str   # Order is important
    confirm_password: str
    roles: Optional[List[Roles]] = []
    is_active: bool = True

    # @validator(SchemaFields.CONFIRM_PASSWORD)
    # def passwords_match(cls, field_value, values, **kwargs):
    #     if SchemaFields.PASSWORD in values and field_value != values[SchemaFields.PASSWORD]:
    #         raise ValidationError('passwords do not match')
    #     return field_value
    #
    # @validator(SchemaFields.USERNAME)
    # def username_alphanumeric(cls, field_value):
    #     if not field_value.isalnum():
    #         raise ValidationError('must be alphanumeric')
    #     return field_value

    class Config:
        orm_mode = True
