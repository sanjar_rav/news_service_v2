from pydantic_sqlalchemy import sqlalchemy_to_pydantic

from models import User

small_serializer_excludes = ("username", "password", "created_at", "updated_at")
big_serializer_excludes = ("password", "created_at", "updated_at")

PydanticSmallUser = sqlalchemy_to_pydantic(User, exclude=small_serializer_excludes)
PydanticBigUser = sqlalchemy_to_pydantic(User, exclude=big_serializer_excludes)
