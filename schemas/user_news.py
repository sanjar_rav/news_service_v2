from pydantic import BaseModel


class UserNewsList(BaseModel):
    id: int
    user_id: int
    news_id: int
    is_read: bool
    send_status: str

    class Config:
        orm_mode = True
