from pydantic import BaseModel


class UserFirebaseList(BaseModel):
    id: int
    device_type: str
    token: str
    user_id: int
    device_id: str
    is_active: bool
    is_notification: bool

    class Config:
        orm_mode = True


class UserFirebaseUpdate(BaseModel):
    device_type: str = None
    token: str = None
    user_id: int = None
    device_id: str = None
    is_active: bool = None
    is_notification: bool = None


class UserFirebaseCreate(BaseModel):
    device_type: str
    token: str
    user_id: int
    device_id: str
    is_active: bool = None
    is_notification: bool = None