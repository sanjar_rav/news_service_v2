from fastapi import FastAPI
from starlette.requests import Request
from starlette.responses import JSONResponse
from utils.exceptions import ValidationError
from v1.routes import user_router
from v1.news import news_router
from v1.user_firebase import user_firebase_router
from v1.user_news import user_news_router
from fastapi.staticfiles import StaticFiles


app = FastAPI(
    title="User Service",
    description="Template API Gateway of ZK",
    version="0.2.4",
    terms_of_service="http://example.com/terms/",
    contact={
        "name": "ZK",
        "url": "https://zk.uz",
    },
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
)

static_directory = "media"
app.mount("/media", StaticFiles(directory="media"), name="media")

@app.exception_handler(ValidationError)
async def debug_exception_handler(request: Request, exc: ValidationError):
    return JSONResponse(
        status_code=400,
        content={"error": exc.message}
    )


app.include_router(user_router, prefix="/api/v1")
app.include_router(news_router, prefix="/api/v1")
app.include_router(user_news_router, prefix="/api/v1")
app.include_router(user_firebase_router, prefix="/api/v1")
