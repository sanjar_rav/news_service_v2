from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from core.database import get_session
from schemas import (PydanticSmallUser, PydanticBigUser,
                     DeactivationSchema, LoginUserSchema,
                     CreateUserSchema)

from services.user import create_user, get_user_by_username

from models.user import User
from utils.exceptions import ValidationError
from utils.hashing import PasswordHandler

user_router = APIRouter()


@user_router.get("/ping")
async def ping():
    return {"message": "Pong"}


@user_router.post("/sign-up", status_code=201)
async def sign_up(payload: CreateUserSchema, db_session: AsyncSession = Depends(get_session)):
    await create_user(payload, db_session)
    return {"message": "Successfully created"}


@user_router.post("/login")
async def login(payload: LoginUserSchema, db_session: AsyncSession = Depends(get_session)):
    if user := await get_user_by_username(payload.username, db_session):
        # TODO: MayBe raise another exception for not active users?
        if PasswordHandler(payload.password).verify(user.password) and user.is_active:
            return PydanticSmallUser.from_orm(user)
    raise ValidationError("Incorrect username or password")


@user_router.get("/get/{user_id}")
async def get_user(user_id: int, db_session: AsyncSession = Depends(get_session)):
    if user := await db_session.get(User, user_id):
        return PydanticBigUser.from_orm(user)
    # TODO: Error is might be critical!
    raise ValidationError("User doesn't exist")


@user_router.post("/deactivate")
async def deactivate_user(payload: DeactivationSchema, db_session: AsyncSession = Depends(get_session)):
    if user := await db_session.get(User, payload.user_id):
        user.is_active = False
        await db_session.commit()
        return {"success": "Successfully deactivated"}
    # TODO: Error is might be critical!
    raise ValidationError("User doesn't exist")
