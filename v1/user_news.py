from sqlalchemy.ext.asyncio import AsyncSession
from fastapi import APIRouter, Depends, Query
from core.database import get_session
from models.news import UserNews
from pydantic.types import List
from sqlalchemy import select
from schemas.user_news import UserNewsList

user_news_router = APIRouter(
    prefix="/user-news",
    tags=["user news"]
)


@user_news_router.get("/", response_model=List[UserNewsList])
async def get_user_news(page: int = Query(1, gt=0),
                        page_size: int = Query(10, gt=0),
                        session: AsyncSession = Depends(get_session)
                        ):
    stmt = select(UserNews)
    result = await session.execute(stmt)
    news = result.scalars().all()

    # pagination
    start_index = (page - 1) * page_size
    end_index = start_index + page_size
    paginated_news = news[start_index:end_index]

    return paginated_news


@user_news_router.get("/{id}/")
async def get_current_user_news(usernews_id: int, session: AsyncSession = Depends(get_session)):
    stmt = select(UserNews).where(UserNews.id == usernews_id)
    result = await session.execute(stmt)
    news = result.scalar_one_or_none()

    if news is None:
        return {"message": "UserNews not found"}

    return news
