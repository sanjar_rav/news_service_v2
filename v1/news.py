from sqlalchemy.ext.asyncio import AsyncSession
import os
from fastapi import APIRouter, Depends, Query
from core.database import get_session
from models.news import News, UserNews
from schemas.news import NewsList, NewsUpdate, SimpleModel
from pydantic.types import List
from sqlalchemy import select, update, delete
from fastapi import UploadFile, Request
from datetime import timedelta
from tasks.celery import create_news_task
from sqlalchemy import desc

CURRENT_HOST = 'http://192.168.31.138:8003'

news_router = APIRouter(
    prefix="/news",
    tags=["news"]
)


@news_router.post("/")
async def create_news(request: Request, news: SimpleModel = Depends(), media: UploadFile = None,
                      db_session: AsyncSession = Depends(get_session)):
    db_news = News(
        title=news.title,
        description=news.description,
        status=news.status,
        send_date=news.send_date,
        send_now=news.send_now,
    )

    if media:
        file_path = os.path.join("media", media.filename)
        current_host = request.base_url
        # url = f'{current_host.scheme}://{current_host.hostname}:{current_host.port}/'

        # Создаем директорию "media", если она не существует
        media_folder = "media"
        if not os.path.exists(media_folder):
            os.makedirs(media_folder)

        # Открываем файл для записи в бинарном режиме и записываем содержимое файла media.file в него
        with open(file_path, "wb") as file:
            file.write(media.file.read())

        # Создаем новый путь к файлу, добавляя префикс "media_" к имени файла media.filename
        new_file_path = os.path.join("media", f'media_{media.filename}')
        # Переименовываем файл, перемещая его из исходного пути file_path в новый путь new_file_path
        os.rename(file_path, new_file_path)

        # Формируем URL-адрес файла, объединяя базовый URL-адрес и новый путь файла
        db_news.media = f'{current_host}{new_file_path}'

    db_session.add(db_news)
    await db_session.commit()

    if db_news.status == 'publish':
        if db_news.send_now is True:
            create_news_task.apply_async(args=(db_news.id,))
        elif db_news.send_date is not None:
            create_news_task.apply_async(args=(db_news.id,), eta=db_news.send_date - timedelta(hours=5))

    return db_news


@news_router.get("/", response_model=List[NewsList])
async def get_news(page: int = Query(1, gt=0),
                   page_size: int = Query(10, gt=0),
                   session: AsyncSession = Depends(get_session)
                   ):
    stmt = select(News).order_by(desc(News.id))
    result = await session.execute(stmt)
    news = result.scalars().all()

    # pagination
    start_index = (page - 1) * page_size
    end_index = start_index + page_size
    paginated_news = news[start_index:end_index]

    return paginated_news


@news_router.get("/{id}/")
async def get_current_news(news_id: int, session: AsyncSession = Depends(get_session)):
    stmt = select(News).where(News.id == news_id)
    result = await session.execute(stmt)
    news = result.scalar_one_or_none()

    if news is None:
        return {"message": "News not found"}

    return news


@news_router.patch("/{id}/")
async def patch_event(news_id: int, news_update: NewsUpdate,
                      session: AsyncSession = Depends(get_session)):
    stmt = update(News).where(News.id == news_id).values(**news_update.dict(exclude_unset=True))
    await session.execute(stmt)
    await session.commit()
    return news_update


@news_router.put("/{id}/")
async def put_event(news_id: int, news_update: NewsUpdate,
                    session: AsyncSession = Depends(get_session)):
    stmt = update(News).where(News.id == news_id).values(**news_update.dict(exclude_unset=True))
    await session.execute(stmt)
    await session.commit()
    return news_update


@news_router.delete("/{id}/")
async def delete_news(news_id: int, session: AsyncSession = Depends(get_session)):
    stmt = delete(UserNews).where(UserNews.news_id == news_id)  # Удалить связанные строки из таблицы "usernews"
    await session.execute(stmt)

    stmt = delete(News).where(News.id == news_id)
    await session.execute(stmt)

    await session.commit()
    return {"results": "success"}
