from sqlalchemy.ext.asyncio import AsyncSession
from fastapi import APIRouter, Depends, Query
from core.database import get_session
from models.news import UserFirebaseDevice
from pydantic.types import List
from sqlalchemy import select, update, delete, insert
from schemas.user_firebase import UserFirebaseList, UserFirebaseCreate, UserFirebaseUpdate
from sqlalchemy import desc

CURRENT_HOST = 'http://192.168.31.138:8003'

user_firebase_router = APIRouter(
    prefix="/user_firebase",
    tags=["user firebase"]
)


@user_firebase_router.get("/", response_model=List[UserFirebaseList])
async def get_user_firebase(page: int = Query(1, gt=0),
                            page_size: int = Query(10, gt=0),
                            session: AsyncSession = Depends(get_session)
                            ):
    stmt = select(UserFirebaseDevice).order_by(desc(UserFirebaseDevice.id))
    result = await session.execute(stmt)
    user_firebase = result.scalars().all()

    # pagination
    start_index = (page - 1) * page_size
    end_index = start_index + page_size
    paginated_news = user_firebase[start_index:end_index]

    return paginated_news


@user_firebase_router.get("/{id}/")
async def get_current_user_firebase(user_firebase_id: int, session: AsyncSession = Depends(get_session)):
    stmt = select(UserFirebaseDevice).where(UserFirebaseDevice.id == user_firebase_id)
    result = await session.execute(stmt)
    user_firebase = result.scalar_one_or_none()

    if user_firebase is None:
        return {"message": "News not found"}

    return user_firebase


@user_firebase_router.post("/")
async def create_user_firebase(new_user_firebase: UserFirebaseCreate, session: AsyncSession = Depends(get_session)):
    stmt = insert(UserFirebaseDevice).values(**new_user_firebase.dict())
    await session.execute(stmt)
    await session.commit()
    return new_user_firebase


@user_firebase_router.patch("/{id}/")
async def patch_event(user_firebase_id: int, user_firebase_update: UserFirebaseUpdate,
                      session: AsyncSession = Depends(get_session)):
    stmt = update(UserFirebaseDevice).where(UserFirebaseDevice.id == user_firebase_id).values(
        **user_firebase_update.dict(exclude_unset=True))
    await session.execute(stmt)
    await session.commit()
    return user_firebase_update


@user_firebase_router.put("/{id}/")
async def put_event(user_firebase_id: int, user_firebase_update: UserFirebaseUpdate,
                    session: AsyncSession = Depends(get_session)):
    stmt = update(UserFirebaseDevice).where(UserFirebaseDevice.id == user_firebase_id).values(
        **user_firebase_update.dict(exclude_unset=True))
    await session.execute(stmt)
    await session.commit()
    return user_firebase_update


@user_firebase_router.delete("/{id}/")
async def delete_news(user_firebase_id: int, session: AsyncSession = Depends(get_session)):
    stmt = delete(UserFirebaseDevice).where(UserFirebaseDevice.id == user_firebase_id)
    await session.execute(stmt)
    await session.commit()
    return {"results": "success"}
