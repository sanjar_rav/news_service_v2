from pydantic import BaseSettings
import os
from core.config import DB_USER, DB_PASS, DB_PORT, DB_NAME, DB_HOST


class Settings(BaseSettings):
    # DATABASE_URL: str = os.getenv("DATABASE_URL", "postgresql+asyncpg://postgres:1@postgres-users:5432/users")
    DATABASE_URL: str = os.getenv("DATABASE_URL",
                                  f"postgresql+asyncpg://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}")


settings = Settings()
