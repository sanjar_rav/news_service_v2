from celery import Celery
import asyncio
from core.database import async_session
from models import User
from models.news import UserNews, News, UserFirebaseDevice
from sqlalchemy import select
from services.user_firebase import send_message_firebase

app = Celery('tasks', broker='redis://localhost:6379/0', backend='redis://localhost:6379/0')
app.conf.task_routes = {
    'tasks.create_news_task': {'queue': 'news_queue'},
}


@app.task(bind=True)
def create_news_task(self, news_id):
    loop = asyncio.get_event_loop()
    loop.run_until_complete(creating_news_for_users(news_id))

    tokens, news_title, news_description = loop.run_until_complete(all_users_with_notification(news_id))
    try:
        loop.run_until_complete(send_message_firebase(news_title, news_description, tokens))
    except:
        pass

    return {'result': 'success'}


async def creating_news_for_users(news_id):
    async with async_session() as session:
        q = select(User)
        result = await session.execute(q)
        users = result.scalars()

        for user in users:
            user_news = UserNews(user_id=user.id, news_id=news_id, send_status='waiting')
            session.add(user_news)
        await session.commit()


async def all_users_with_notification(news_id):
    async with async_session() as session:
        q = select(UserFirebaseDevice).where(UserFirebaseDevice.is_notification == True)
        result = await session.execute(q)
        users = result.scalars()

        tokens = [user.token for user in users]

        # text and description of news
        stmt = select(News).where(News.id == news_id)
        result = await session.execute(stmt)
        news = result.scalar_one_or_none()
        news_title = news.title
        news_description = news.description

        return tokens, news_title, news_description
