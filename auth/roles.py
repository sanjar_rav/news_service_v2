from enum import Enum


class Roles(str, Enum):
    SUPER_ADMIN = 'super_admin'
    ADMIN = 'admin'
