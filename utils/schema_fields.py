class SchemaFields:
    """
    MetaClass for dynamic creation of SchemaFields

    class DynamicFields(metaclass=SchemaFields):
        class Meta:
            schema = pydantic.BaseModel
    """

    def __new__(mcs, *args, **kwargs):
        class_ = super().__new__(mcs, *args, **kwargs)

        try:
            meta_class = mcs.Meta
        except AttributeError:
            raise AttributeError("Class should have Meta class")

        try:
            schema = meta_class.schema
        except AttributeError:
            raise AttributeError("Meta class should have schema field")

        try:
            for field in schema.__fields__:
                class_.__setattr__(field.upper(), field)

        except AttributeError:
            raise AttributeError("Field `schema` should have class type of `pydantic.BaseModel` as value")

        return class_
