class ValidationError(Exception):
    """
    Validation errors in current service (API GATEWAY)
    """
    def __init__(self, message):
        super().__init__()
        self.message = message
