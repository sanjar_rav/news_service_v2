from passlib.context import CryptContext


pwd_context = CryptContext(schemes=["bcrypt"],
                           deprecated="auto")


class PasswordHandler:
    """
    Class for handling password (hashing and verifying)
    """
    def __init__(self, plain_password):
        self._plain_password = plain_password

    def verify(self, hashed_password: str) -> bool:
        return pwd_context.verify(self._plain_password, hashed_password)

    @property
    def hashed(self) -> str:
        return pwd_context.hash(self._plain_password)
