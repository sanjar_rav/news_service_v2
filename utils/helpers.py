def get_clean_path(*args, symbol: str = "/") -> str:
    """
    Example:
        get_clean_path(http://127.0.0.1:8000, api/v1/, /path/) -> http://127.0.0.1:8000/api/v1/path/
    :param symbol - join arguments with given symbol (in terms of url, it should be "/")
    """
    return symbol.join(
        [arg.rstrip(symbol).lstrip(symbol) for arg in args]
    )
