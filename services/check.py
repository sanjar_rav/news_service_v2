from sqlalchemy import exists
from sqlalchemy.ext.asyncio import AsyncSession

from models import User


async def username_exists(username: str, db_session: AsyncSession) -> bool:
    query_result = await db_session.execute(exists(User).select().where(User.username == username))
    return query_result.scalar()
