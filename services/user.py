from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from models import User
from schemas.sign_up import CreateUserSchema
from services.check import username_exists
from utils.exceptions import ValidationError
from utils.hashing import PasswordHandler
import random


async def create_user(payload: CreateUserSchema, db_session: AsyncSession) -> None:
    username: str = payload.username + str(random.randint(1, 10000))

    if await username_exists(username, db_session):
        raise ValidationError(f"Username `{username}` already exists")

    hashed_password = PasswordHandler(payload.password).hashed
    db_session.add(
        User(username=username,
             password=hashed_password,
             is_active=payload.is_active,
             roles=payload.roles)
    )
    await db_session.commit()


async def get_user_by_username(username: str, db_session) -> User:
    instance = await db_session.execute(select(User).where(User.username == username))
    return instance.scalar()



