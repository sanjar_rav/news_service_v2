import requests
import json


def send_message_firebase(title, msg, registration_tokens):
    max_tokens_per_request = 1000
    num_tokens = len(registration_tokens)
    num_requests = (num_tokens // max_tokens_per_request) + 1
    request_code_list = []

    for i in range(num_requests):
        start = i * max_tokens_per_request
        end = min((i + 1) * max_tokens_per_request, num_tokens)
        tokens_batch = registration_tokens[start:end]

        print('//////////////////////////////////////////////////////')
        print(tokens_batch)
        print('//////////////////////////////////////////////////////')

        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'key=' + "AAAAOUzqo-A:APA91bHrivGitjVFBitnbuN2kO1btncA0NmXH28bfqjyutN4k2-GlbJ68Z1_30zZEB2nq0zur9wrV0rCNMQP1iGk2cZpmpHGdm-VUw54BgWbofs9V9fGWGXc9Xx12xN9yJnTTihwRzTd",
        }
        body = {
            "registration_ids": tokens_batch,
            "notification": {
                'title': title,
                "body": msg
            }
        }
        request = requests.post("https://fcm.googleapis.com/fcm/send", headers=headers, data=json.dumps(body))
        data = json.loads(request.text)
        print('title: ', title)
        print('body: ', msg)

        for j, token in enumerate(tokens_batch):
            request_code_list.append({
                'token': token,
                'status_code': request.status_code,
                'success_count': data['success'],
                'failure_count': data['failure'],
            })
        print(request_code_list)

    print('firebase is working')
    return request_code_list
