from datetime import datetime
from sqlalchemy import Boolean, Column, Integer, String, DateTime, Enum, Text, ForeignKey, LargeBinary
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import relationship
from models import User

Base = declarative_base()


class News(Base):
    __tablename__ = "news"

    id = Column(Integer, primary_key=True, index=True)
    created_at = Column(DateTime, default=datetime.now, nullable=False)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now, nullable=False)
    title = Column(String)
    description = Column(Text)
    media = Column(String, nullable=True)
    status = Column(Enum('draft', 'archive', 'publish', name='status'), default='publish')
    send_date = Column(DateTime)
    send_now = Column(Boolean, default=True)

    user_news = relationship("UserNews", back_populates="news")


class UserNews(Base):
    __tablename__ = "usernews"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey(User.id))
    news_id = Column(Integer, ForeignKey('news.id'))
    is_read = Column(Boolean, default=False)
    send_status = Column(Enum('waiting', 'in_progress', 'success', 'failed', name='send-status'), default='waiting')

    user = relationship(User)
    news = relationship(News)


class UserFirebaseDevice(Base):
    __tablename__ = "user_firebase_device"

    id = Column(Integer, primary_key=True, index=True)
    device_type = Column(Enum('android', 'ios', name='device-type'), default='android')
    token = Column(String)
    user_id = Column(Integer, ForeignKey(User.id))
    device_id = Column(String)
    is_active = Column(Boolean, default=True)
    is_notification = Column(Boolean, default=True)

    user = relationship(User)
