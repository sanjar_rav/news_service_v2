from datetime import datetime
from sqlalchemy import Boolean, Column, Integer, String, DateTime, Enum, ARRAY
from sqlalchemy.orm import declarative_base
from sqlalchemy.ext.mutable import MutableList
from auth.roles import Roles

Base = declarative_base()


# TODO: add first_name, last_name
class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True, nullable=False)
    password = Column(String, nullable=False)
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=datetime.now, nullable=False)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now, nullable=False)
    roles = Column(MutableList.as_mutable(ARRAY(Enum(Roles))), default=[])
    is_notification = Column(Boolean, default=True)
    token = Column(String, unique=True, nullable=True)
